import React, { PropTypes, Component } from 'react';
import {
  FormLabel
} from 'react-native-elements'
import FormInput from '../form-input';
import { View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements'

export default class Password extends React.Component {
  state = {
    securePassword: true
  }

  render() {
    const { labels, customStyles, onChangeText, value } = this.props
    const { securePassword } = this.state;
    return (
      <View>
        <FormLabel
          labelStyle={{ color: customStyles.textColor }}
        >
          {labels.password}
        </FormLabel>
        <FormInput
          inputStyle={{ color: customStyles.textColor }}
          containerStyle={{ borderBottomColor: customStyles.textColor }}
          required
          valid={password => password.length >= 1}
          message={labels.passwordLenghtValidation}
          value={value}
          autoCapitalize='none'
          onChangeText={onChangeText}
          messageStyle={{ color: customStyles.validationTextColor }}
          keyboardAppearance={customStyles.keyboardAppearance}
          selectionColor={customStyles.textColor}
          underlineColorAndroid={customStyles.textColor}
          secureTextEntry={securePassword} />
        <TouchableOpacity
          style={{ position: 'absolute', top: 40, right: 20 }}
          onPress={() => this.setState({ securePassword: !securePassword })} >
          <Icon name={securePassword ? 'visibility' : 'visibility-off'} color={customStyles.visibilityColor || 'white'} />
        </TouchableOpacity>
      </View>
    );
  }
}
